import {useState, useEffect} from 'react';
import { Card, Button, } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({course}) {

	const { name, description, price, _id } = course;

	function enroll() {
		
	}

	return (
		
			<Card class="text-center" md={3} className="bg-info">
			    <Card.Body>
			        <Card.Title class="text-center">{name}</Card.Title>
			        <Card.Subtitle Class="text-center">Description:</Card.Subtitle>
			        <Card.Text class="text-center">{description}</Card.Text>
			        <Card.Subtitle class="text-center">Price:</Card.Subtitle>
			        <Card.Text class="text-center">PhP {price}</Card.Text>
			        <div class="text-center">
			        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Checkout</Button>
			        </div>
			    </Card.Body>
			</Card>
		
	)	
};

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
