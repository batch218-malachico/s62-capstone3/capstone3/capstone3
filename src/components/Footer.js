import React from 'react';
import {Container} from 'react-bootstrap';

export default function Highlights() {

	 return (
	 <Container>
	    <div className="text-center">
	      <footer>
	      	<h4>All Rights Reserve</h4>
	        <p>Copyright © {new Date().getFullYear()} Code Pizza</p>
	      </footer>
	    </div>
	  </Container>
	  );
	
}